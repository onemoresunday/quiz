#!/bin/sh

python manage.py migrate
python manage.py collectstatic --noinput

gunicorn -w ${WSGI_WORKERS} -b 0:${WSGI_PORT} --chdir ./quiz  quiz_main.wsgi:application --log-level=${WSGI_LOG_LEVEL}
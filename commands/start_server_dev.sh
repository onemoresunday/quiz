#!/bin/sh

python manage.py migrate --settings=quiz_main.settings.${MODE}
python manage.py collectstatic --noinput --settings=quiz_main.settings.${MODE}
python manage.py runserver --settings=quiz_main.settings.${MODE} 0:${WSGI_PORT}

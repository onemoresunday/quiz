from rest_framework.serializers import ModelSerializer
from accounts.models import CustomUser
from quiz.models import Question, Choice, Quiz


class UserSerializer(ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ["title", "first_name", "last_name", "email", "is_staff"]


class ChoiceSerializer(ModelSerializer):
    class Meta:
        model = Choice
        fields = ("id", "text", "is_correct")


class QuestionDetailSerializer(ModelSerializer):
    choices = ChoiceSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = ["id", "order_number", "text", "choices"]


class QuizSerializer(ModelSerializer):
    class Meta:
        model = Quiz
        fields = ["id", "title", "level", "description", "questions_count"]

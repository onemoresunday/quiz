from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers, permissions

from api.views import UserViewSet, QuestionDetailView, QuizListView

router = routers.DefaultRouter()
router.register(r"custom-users", UserViewSet)

schema_view = get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version="v1",
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[
        permissions.AllowAny,
    ],
)

urlpatterns = [
    # path("", include(router.urls)),
    # path("auth/", include('rest_framework.urls')),
    path("docs/", schema_view.with_ui("swagger", cache_timeout=0), name="swagger_docs"),
    path("auth/", include("djoser.urls")),
    path("auth/jwt/", include("djoser.urls.jwt")),
    path(
        "quiz/<uuid:uuid>/questions/<int:order_number>/",
        QuestionDetailView.as_view(),
        name="question_detail",
    ),
    path("quiz/", QuizListView.as_view(), name="quiz_list"),
]

import datetime

from mongoengine import (
    Document,
    EmbeddedDocument,
    StringField,
    ListField,
    EmbeddedDocumentField,
    DateTimeField,
)


class Blog(EmbeddedDocument):
    name = StringField(max_length=128)
    tagline = StringField()


class Entry(Document):
    blog = ListField(EmbeddedDocumentField(Blog))
    timestamp = DateTimeField(default=datetime.datetime.now)
    headline = StringField(max_length=255)

from django.contrib import admin
from quiz.models import Quiz, Choice, Result, Question


admin.site.register([Quiz, Choice, Result, Question])
